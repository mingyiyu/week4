# Tweetscraper  

Based on an initial query of Twitter's database, a chain of related hashtags (or user mentions) is assembled through recursive searches made based on the results of prior searches. The various versions of the script implement different conditions for halting and extending the recursive procedure. Three sets of results are saved: (1) data from tweets are saved in a .csv file, (2) a count of hashtags (and user mentions) from results is saved in a .csv file, (3) the chain of terms queried is logged in a .txt file.  

## tweetscraper_v1  
From the results of a query, the most frequently tweeted hashtag that has not already previously been queried is selected as the next query term. This process is repeated until the initially queried term is not returned in the results of a query.  

Earlier implementations of this algorithm made a count of all tweeted hashtags. This was later amended to exclude the queried term from the count of hashtags. As the queried term was the basis of the search, it would be present in each tweet in the results, leading to a count disproportionately weighted towards the terms that were queried. It is hoped that this amendment makes the hashtag count more useful (although, admittedly, the precise implications of this count are not particularly apparent, even to me).  

This version of the script was usually unable to accrue any momentum: the series of queries rarely exceeded five terms. (Perhaps a result of my lack of imagination in the selection of terms?) Intriguingly, the exception to this were terms related to Donald Trump ("#trump", "#maga", "#trumptrain") -- in this expression of popular will, at least, he holds a significant advantage over "#hillary".  

### tweetscraper_v2  
Similar to v1 of the script, v2 selects a new term to query from the most frequently tagged term in the results that had not already been queried. However, this process is instead repeated until the previously queried term (rather than the first) was not found in the results.  

The premise behind halting the recursive search on the absence of the initial query in the returned results was that this would indicate that the chain had drifted from the initial topic (i.e. that the new query was no longer related to the initial one, and hence its results would not include the initially queried term). In fact, however, the chain often halted even on terms that seemed to be of conceptual relevance. (For example, on search 20 [see log_v1.txt], any interest in "#debate" was quickly foreclosed -- "#maga".) My hope had been that this algorithm would excavate the set of privileged keywords around which discourse coagulates. Unfortunately, I seem to have overestimated the significance of the hashtag as a cultural phenomenon; even a cursory scan through the returned tweets shows that, while by no means completely absent, the usage of hashtags was hardly pervasive. 

In the interest of expanding the range of results, user mentions were thus included in the count of tags. The latter appear to be as popular as hashtags (if not more so).  

### tweetscraper_v3  
Unlike v1 and v2, v3 generates a list of new terms to query on the next round of searches (I decided on the top 3 not previously searched for terms). Data from all queries on each round is collated before new terms are selected. The procedure halts on the absence of the initial term from the collated results.  

It should be noted that while each version attempts to respond to perceived deficiencies in prior versions, the later versions should not be seen as superseding the earlier ones. Each simply results in a different type of series: while v2 might indeed have succeeded in generating longer chains of search terms, the coherence of each series is questionable -- in this sense, it is less like the associative trails envisioned by Vannevar Bush when conceptualizing the memex, and more comparable to a chance-based Surrealist experiment.  

## Implementation details  
The save_tweets function performs the higher-level operations of file input/output, accepting the initial query term as its sole argument. In addition, it sets in motion the search_twitter function that serves as the turbine of the script.  
The search_twitter function performs queries to the Twitter API, accumulating results (and writing data to an open .csv file if passed one by the save_tweets function). Based on the results, it either returns: (1) the count of hashtags found in the searches as a dictionary and the series of terms queried as a string, or (2) a recursive call to itself with new term(s) to query and the data accumulated thus far.  
Most of the other functions are helper functions that abstract straightforward aspects of the script for clarity and potentially for code reuse: the exception is the next_query function, which determines the new term (or list of terms) to be queried on the next call of the search_twitter function.  